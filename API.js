//npm install frisby --save-dev
//npm install --save-dev jest
//mkdir -p __tests__/api
//touch __tests__/api/api_spec.js
//jest

const frisby = require('frisby');

it('should be a teapot', function () {
  return frisby.get('http://httpbin.org/status/418')
    .expect('status', 418);
});
